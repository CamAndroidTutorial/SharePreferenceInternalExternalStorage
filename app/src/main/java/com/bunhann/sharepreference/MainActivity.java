package com.bunhann.sharepreference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout edName;
    private EditText edContent;
    private Button btnSave, btnReadFile;

    SharedPreferences pref;

    static final String my_name = "MY_NAME";

    static final int READ_BLOCK_SIZE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edName = (TextInputLayout) findViewById(R.id.edName);
        edContent = (EditText) findViewById(R.id.edReadme);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnReadFile = (Button) findViewById(R.id.btnRead);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // add-write text into file
                try {
                    FileOutputStream fileout=openFileOutput("mytextfile.txt", MODE_PRIVATE);
                    OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
                    outputWriter.write(edContent.getText().toString());
                    outputWriter.close();

                    //display file saved message
                    Toast.makeText(getBaseContext(), "File saved successfully!",
                            Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnReadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //reading text from file
                try {
                    FileInputStream fileIn=openFileInput("mytextfile.txt");
                    InputStreamReader InputRead= new InputStreamReader(fileIn);

                    char[] inputBuffer= new char[READ_BLOCK_SIZE];
                    String s="";
                    int charRead;

                    while ((charRead=InputRead.read(inputBuffer))>0) {
                        // char to string conversion
                        String readstring=String.copyValueOf(inputBuffer,0,charRead);
                        s +=readstring;
                    }
                    InputRead.close();
                    edContent.setText(s);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        // com_bunhann_sharepreference_preference.xml

        //pref = this.getSharedPreferences("Activity1", MODE_PRIVATE);
        // setting.xml
        String strName = pref.getString(my_name, "");

        if (strName.length()>0){
            edName.getEditText().setText(strName);
        }


    }

    @Override
    protected void onPause() {

        SharedPreferences.Editor preferencesEditor = pref.edit();

        preferencesEditor.putString(my_name, edName.getEditText().getText().toString());

        preferencesEditor.apply();

        super.onPause();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_action_usa:
                //Change Application level locale
                LocaleHelper.setLocale(MainActivity.this, "en");
                //It is required to recreate the activity to reflect the change in UI.
                recreate();
                return true;
            case R.id.menu_action_km:
                //Change Application level locale
                LocaleHelper.setLocale(MainActivity.this, "km");
                //It is required to recreate the activity to reflect the change in UI.
                recreate();
                return true;
            case R.id.menu_action_read_write_activity:

                Intent intent = new Intent(this, ReadWriteFileActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
